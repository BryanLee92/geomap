var map;
var mapcanvas;

let coordinates = [];

var script = document.createElement('script');
script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDKtpfBWHfq3S_9s_LKCHfoTbKmzvfImZo&libraries=drawing&callback=initMap';
script.async = true;

function initMap() {
    var location = new google.maps.LatLng(3.081833,101.583051);
    map0ptions ={
        center: location,
        zoom: 15,
        mapTypeId:google.maps.MapTypeId.RoadMap
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), map0ptions );
    var all_overlays= [];
    var selectedShape;
    var drawingManager = new google.maps.drawing.DrawingManager({
        drawingControlOptions:{
            position:google.maps.ControlPosition.Top_Center,
            drawingModes:[
                google.maps.drawing.OverlayType.POLYGON,
                // Extra options:
                // google.maps.drawing.OverlayType.MARKER,
                // google.maps.drawing.OverlayType.CIRCLE,
                // google.maps.drawing.OverlayType.RECTANGLE,
            ]
        },
        polygonOptions:{
            clickable:true,
            draggable:false,
            editable:true,
            fillColor:'#ADFF2F',
            fillOpacity:0.3
        },
        // Extra options:
        // circleOptions:{
        //     fillColor:'#ADFF2F',
        //     fillOpacity:0.2,
        //     strokeWeight:3,
        //     clickable:true,
        //     editable:true
        // },
        // rectangleOptions:{
        //     fillColor:'#ADFF2F',
        //     fillOpacity:0.2,
        //     strokeWeight:3,
        //     clickable:true,
        //     editable:true
        // },
    })

    drawingManager.setMap(map);

    function clearSelection() {
        if (selectedShape) {
            selectedShape.setEditable(false);
            selectedShape = null;
        }
    }
    //to disable drawing tools
    function stopDrawing() {
        drawingManager.setMap(null);
    }

    function setSelection(shape) {
        clearSelection();
        stopDrawing()
        selectedShape = shape;
        shape.setEditable(true);
    }

    function deleteSelectedShape() {
        if (selectedShape) {
            selectedShape.setMap(null);
            drawingManager.setMap(map);
            coordinates.splice(0, coordinates.length)
            document.getElementById('info').innerHTML = ""
        }
    }

    function CenterControl(controlDiv, map) {
        
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = 'rgb(50,50,50)';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Select to delete the shape';
        controlDiv.appendChild(controlUI);

        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(0,255,255)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = 'Delete Selected Area';
        controlUI.appendChild(controlText);

        //to delete the polygon
        controlUI.addEventListener('click', function () {
            deleteSelectedShape();
        });
    }

    var getPolygonCoords = function (newShape) {
        coordinates.splice(0, coordinates.length)
        var len = newShape.getPath().getLength();
        for (var i = 0; i < len; i++) {
            coordinates.push(newShape.getPath().getAt(i).toUrlValue(6))
        }
        document.getElementById('info').innerHTML = coordinates
    }

    google.maps.event.addListener(drawingManager, 'polygoncomplete', function (event) {
        event.getPath().getLength();
        google.maps.event.addListener(event, "dragend", getPolygonCoords(event));

        google.maps.event.addListener(event.getPath(), 'insert_at', function () {
            getPolygonCoords(event)
        });

        google.maps.event.addListener(event.getPath(), 'set_at', function () {
            getPolygonCoords(event)
        })
    })

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
        all_overlays.push(event);
        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
            drawingManager.setDrawingMode(null);

            var newShape = event.overlay;
            newShape.type = event.type;
            google.maps.event.addListener(newShape, 'click', function () {
                setSelection(newShape);
            });
            setSelection(newShape);
        }
    })

    var centerControlDiv = document.createElement('div');
    var centerControl = new CenterControl(centerControlDiv, map);
    
    centerControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
};

document.head.appendChild(script);